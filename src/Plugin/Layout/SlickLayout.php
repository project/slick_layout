<?php

namespace Drupal\slick_layout\Plugin\Layout;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutDefault;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Slick Layout Class.
 */
class SlickLayout extends LayoutDefault implements PluginFormInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'slick_optionset' => 'default',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $slick_optionsets = [];

    $config_factory = \Drupal::configFactory();
    foreach ($config_factory->listAll('slick.optionset.') as $optionset_name) {
      $optionset = $config_factory->getEditable($optionset_name);
      $slick_optionsets[$optionset->get('id')] = $optionset->get('label');
    }

    $configuration = $this->getConfiguration();
    $form['slick_optionset'] = [
      '#type' => 'select',
      '#title' => $this->t('Optionset'),
      '#default_value' => $configuration['slick_optionset'],
      '#options' => $slick_optionsets,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Any additional form validation that is required.
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['slick_optionset'] = $form_state->getValue('slick_optionset');
  }

  /**
   * {@inheritdoc}
   */
  public function build(array $regions) {
    $build = parent::build($regions);

    $route_name = \Drupal::routeMatch()->getRouteName();
    $isAdminRoute = \Drupal::service('router.admin_context')->isAdminRoute();
    $isLayoutParagraphBuilderRoute = strpos($route_name, 'layout_paragraphs.builder.') === 0;

    if ($isAdminRoute || $isLayoutParagraphBuilderRoute) {
      return $build;
    }

    $configuration = $this->getConfiguration();
    $slick_optionset = $configuration['slick_optionset'];

    $items = [];
    foreach ($build['slides'] as $id => $slide) {
      if (strpos($id, '#' === 0)) {
        continue;
      }
      $items[] = [
        'slide' => [
          '#type' => 'container',
          'content' => $slide,
        ],
      ];
      unset($build['slides'][$id]);
    }

    $slick = \Drupal::service('slick.manager');
    $element = $slick->build([
      'settings' => [
        'optionset' => $slick_optionset,
      ],
      'items' => $items,
    ]);

    $build['slides'][] = $element;

    return $build;
  }

}
