Module: Slick Layout
Author: David Fröhlich


Description
===========
Adds a "Slick" layout type to your website, that allows you to
show it's contents as a Slick slider.


Installation
============
Copy the 'slick_layout' module directory in to your Drupal 'modules'
directory as usual.


Usage
=====
When using the Layout Builder, select the "Slick" layout type.
Next, use the "Layout Options" to specify the slick optionset.
